/*
* File Name: buffer.c
* Compiler: MS VS 2015
* Authour: Brandon Gould 040 872 459
* Assignment: 1
* Date: 2019-02-04
* Professor: Sv. Ranev
* Purpose: Buffer Utility with function definitions. Three modes: fixed size, additive self incrementing, and multiplicative self incrementing.
* Function List: b_allocate(), b_addc(), b_clear(), b_free(), b_isfull(),
*	b_limit(), b_capacity(), b_mark(), b_mode(), b_incfactor(), b_load(),
b_isempty(), b_getc(), b_eob(), b_print(), b_compact(), b_rflag(),
b_retract(), b_reset(), b_getcoffset(), b_rewind(), b_location()
*/

#include "buffer.h"

/*
* Purpose: Creates a new buffer in memory.
* Author: Brandon Gould
* Version: 0.1
* Called Functions: calloc(), malloc(), b_free()
* Parameters: short init_capacity, char inc_factor, char o_mode (f, a, m)
* Return Value: Buffer* on success, NULL on failure.
* Algorithm: Check validity of capacity. Allocate Buffer Pointer. Allocate Buffer with or based on init_capacity.
	Set inc factor and mode based on inc_factor and o_mode. Set capacity and flags.
*/
Buffer * b_allocate(short init_capacity, char inc_factor, char o_mode)
{
	Buffer * newBuffer; /* Temp Buffer Pointer */
	/* Validity of capacity */
	if (init_capacity < 0 || init_capacity == SHRT_MAX) {
		return NULL;
	}
	/* Allocate for Buffer Pointer */
	newBuffer = (Buffer *)calloc(1, sizeof(Buffer));
	if (!newBuffer) {
		return NULL;
	}

	/* Allocate Buffer with or based on init_capacity */
	if (init_capacity == 0) {
		newBuffer->cb_head = malloc(DEFAULT_CAP); 
		if (o_mode == 'a' || o_mode == 'm') {
			inc_factor = DEFAULT_INC;
		}
		else if (o_mode == 'f') {
			inc_factor = 0;
		}
	}
	else {
		newBuffer->cb_head = malloc(init_capacity);
	}
	if (!newBuffer->cb_head) {
		free(newBuffer);
		return NULL;
	}
	/* Set mode and Inc_Factor */
	if (o_mode == 'f' || (inc_factor == 0 && init_capacity != 0)) {
		newBuffer->mode = 0;
		newBuffer->inc_factor = 0;
	}
	else if (o_mode == 'a' && (unsigned char)inc_factor > 0 && (unsigned char)inc_factor <= A_MAX) {
		newBuffer->mode = 1;
		newBuffer->inc_factor = inc_factor;
	}
	else if (o_mode == 'm' && inc_factor > 0 && inc_factor <= M_MAX) {
		newBuffer->mode = -1;
		newBuffer->inc_factor = inc_factor;
	}
	else { /* Invalid Parameters */
		b_free(newBuffer);
		return NULL;
	}

	if (init_capacity == 0) {
		newBuffer->capacity = DEFAULT_CAP;
	}
	else {
		newBuffer->capacity = init_capacity;
	}

	newBuffer->flags = DEFAULT_FLAGS;
	return newBuffer;
}

/*
* Purpose: Add a chracter to the buffer, allocate room if needed and possible.
* Author: Brandon Gould
* Version: 0.1
* Called Functions: 
* Parameters: pBuffer const pBD, char symbol
* Return Value: Null on failure, buffer pointer on success.
* Algorithm: Reset R flag. Check if buffer is full. If full and mode 0 return null.
* If full and mode 1, if possbile increase capacity by inc_factor, if not enough increase to max.
* If full and mode -1 calculate new values.
* Realloc for full and modes -1/1 if possible. Save head if new address.
* Add symbol at next location and return pointer to buffer on success.
*/
pBuffer b_addc(pBuffer const pBD, char symbol)
{
	short new_space, new_inc, ava_space = 0; /* new_space, new_inc: hold possibly new capacity and inc_factor, respectively. */
											 /* ava_space: to hold space avaliable in the buffer with current capacity and characters added. */
	char* temp_buffer; /* To hold a pointer to a buffer. */
	if (!pBD) {
		return NULL;
	}
	pBD->flags &= RESET_R_FLAG; /* reset r flag*/
								/* TODO check values */
	if ((short)(pBD->addc_offset * sizeof(char)) == pBD->capacity) { /* if buffer full */
		if (pBD->mode == 0) { /* mode 0 returns null */
			return NULL;
		}
		else if (pBD->mode == 1) {
			if (pBD->capacity == (SHRT_MAX_EDGE)) { /* reached max */
				return NULL;
			}
			new_space = (unsigned char)(pBD->inc_factor) + pBD->capacity; /* TODO CAREFUL? */
			if (new_space > (SHRT_MAX_EDGE) && new_space > 0) { /* greater than max-1 and positive */
				new_space = SHRT_MAX_EDGE;
			}
			else if (new_space <= 0) {
				return NULL;
			}
		}
		else if (pBD->mode == -1) {
			if (pBD->capacity == (SHRT_MAX_EDGE)) { /* reached max */
				return NULL;
			}
			ava_space = (SHRT_MAX_EDGE) - pBD->capacity;
			new_inc = ava_space * (short)pBD->inc_factor / 100;
			new_space = pBD->capacity + new_inc;
			if (new_space <= 0 || new_space > (SHRT_MAX_EDGE) || new_inc <= 0) {
				new_space = SHRT_MAX - 1;
			}
			/* .: else new_space valid */

		}
		else {
			return NULL; /* invalid mode */
		}
		/* 1 and -1 modes reach here */
		temp_buffer = realloc(pBD->cb_head, new_space); /* allocate space for a new buffer */
		if (!temp_buffer) {
			return NULL;
		}
		if (temp_buffer != pBD->cb_head) {
			pBD->flags |= SET_R_FLAG;
		}
		pBD->cb_head = temp_buffer;
		pBD->capacity = new_space;
	}

	/* ALL GOOD -> Add symbol & return */
	pBD->cb_head[pBD->addc_offset++] = symbol; /* Add symbol to head at addc_offset, then increment */
	return pBD;
}

/*
* Purpose: Retains the memory space currently allocated to the buffer, but
*	reinitalizes all the appropriate data members of the given Buffer Descriptor,
*	such that the buffer will appear empty. Next b_addc will place at beginning.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer Descriptor
* Return Value: RT_FAIL_1 if Buffer Descriptor null. SUCC(0) if success.
* Algorithm: Check Buffer Descriptor is not null. Reset addc, markc, getc offsets, and reset flags.
*/
int b_clear(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	pBD->addc_offset = 0;
	pBD->markc_offset = 0;
	pBD->getc_offset = 0;
	pBD->flags &= RESET_EOB;
	pBD->flags &= RESET_R_FLAG;
	return 0;
}

/*
* Purpose: De-allocate memory occupied by character buffer and buffer descriptor.
* Author: Brandon Gould
* Version: 0.1
* Called Functions: free()
* Parameters: Buffer Descriptor pointer pBD.
* Return Value:
* Algorithm: If buffer descriptor free cb_head and then buffer descriptor.
*/
void b_free(Buffer * const pBD)
{
	if (pBD) {
		free(pBD->cb_head);
		free(pBD);
	}
}

/*
* Purpose: Returns int based on character buffer being full or not.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters:Buffer Descriptor pointer pBD.
* Return Value: Null buffer returns RT_FAIL_1. Full Buffer returns SUCC_TRUE. Non-full buffer returns 0.
* Algorithm: Check buffer descriptor is not null, return RT fail 1 if is. Check if addc_offset is equal to capacity,
*	addc_offset multiplied by sizeof char incase it's larger. Return SUCC_TRUE if equal size, buffer is full. Otherwise return 0.
*/
int b_isfull(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	else if ((short)(pBD->addc_offset * sizeof(char)) == pBD->capacity) {
		return SUCC_TRUE;
	}
	return 0;
}

/*
* Purpose: Returns the current limit of the character buffer.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: BufferDescriptor pointer const as pBD.
* Return Value: If passed in BufferDescriptor is null will return with RT_FAIL_1. Success returns short addc_offset.
* Algorithm:
*/
short b_limit(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	return pBD->addc_offset;
}

/*
* Purpose: Returns the current capacity of the character buffer.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: RT_FAIL_1 on failure. Short representing current capacity on success.
* Algorithm: Check validity of pBD. Return current capacity.
*/
short b_capacity(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	return pBD->capacity;
}

/*
* Purpose: Set markc_offset to mark. 
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: pBuffer const pBD, short mark
* Return Value: RT_FAIL_1 on failure. Short markc_offset on success.
* Algorithm: Check validity of pBD. Check validity of mark, if good set marc_offset to mark.
*/
short b_mark(pBuffer const pBD, short mark)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	if (mark >= 0 && mark <= pBD->addc_offset) {
		return pBD->markc_offset = mark;
	}
	return RT_FAIL_1;
}

/*
* Purpose: Returns int representation of the mode.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: Casted mode as int. RT_FAIL_2 (-2) on failure.
* Algorithm: Check validity of Buffer *. Check for valid mode, return mode casted as int. RT_FAIL_2 if reaches end, because bad mode.
*/
int b_mode(Buffer * const pBD) {
	if (!pBD) {
		return RT_FAIL_2; /* RT 2(-2) because RT 1(-1) is a mode */
	}
	return (int)pBD->mode;
}

/*
* Purpose: Return the non-negative value of inc_factor.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: unsigned char of inc_factor
* Algorithm: Check validity of Buffer *. Return inc_factor casted to unsigned char.
*/
size_t b_incfactor(Buffer * const pBD) {
	if (!pBD) {
		return RT_FAIL_T;
	}
	return (unsigned char) pBD->inc_factor;
}

/*
* Purpose: Used to read the buffer. 
* Author: Brandon Gould
* Version: 0.1
* Called Functions: fgetc(), b_addc(), ungetc()
* Parameters: FILE * const fi, Buffer * const pBD
* Return Value: (int) -1 on RT errors, -2 on failure to load file, and number of char added to buffer on success
* Algorithm: Keep getting the next character in the file, and attempting to add it to the buffer.
*	Break on end of file. Report failed character. Return count of char added.
*/
int b_load(FILE * const fi, Buffer * const pBD)
{
	char next; /* char to be added to buffer */
	int added = 0; /* count of char added to buffer */
	if (!pBD || !fi) {
		return RT_FAIL_1;
	}
	while (1) {
		next = (char)fgetc(fi); /* cast to char as fgetc somtimes returns int for EOF */
		if (feof(fi)) {
			break;
		}
		if (!b_addc(pBD, next)) {
			ungetc(next, fi);
			printf("Last character read from input file is: %c %d\n", next, (int)next);
			return LOAD_FAIL;
		}
		added++;
	}
	return added;
}

/*
* Purpose: Return based on character buffer being empty or not.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: (int) -1 for RT errors, 1 for empty, 0 for not empty.
* Algorithm: Check validity of Buffer. Check if addc_offset is 0, return 1 if. Otherwise returns 0.
*/
int b_isempty(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	if (pBD->addc_offset == 0) {
		return SUCC_TRUE;
	}
	return 0;
}

/*
* Purpose: Reads the buffer. 
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer Descriptor (Buffer * const) pBD.
* Return Value: (char) -2 for null buffer. 0 for EOB. otherwise returns the next char.
* Algorithm:
*/
char b_getc(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_2;
	}
	/* set flags and return based on values */
	/* if EOB */
	if (pBD->getc_offset == pBD->addc_offset) { 
		pBD->flags |= SET_EOB; /* Set EoB to 1 */
		return AT_EOB;
	}
	pBD->flags &= RESET_EOB; /* Set EoB to 0 */
	return pBD->cb_head[pBD->getc_offset++];
}

/*
* Purpose: Returns value of eob flag.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: Int representing the EOB flag true or false (# or 0).
* Algorithm: Check buffer * const validity. Return EOB flag casted as int.
*/
int b_eob(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	return (int)(pBD->flags & CHECK_EOB); /* Check and return EOB flag */
}

/*
* Purpose: Diagnostic function. Prints character by character the contents of the character buffer to the stdout.
* Author: Brandon Gould
* Version: 0.1
* Called Functions: b_eob(), b_getc()
* Parameters: Buffer * const pBD
* Return Value: RT_FAIL_1 if null Buffer Descriptor. Return 0 on empty buffer. otherwise returns number of char printed.
* Algorithm: Check validity. Check if buffer is empty. If empty return 0.
*	Go through each character, checking for EoB and printing the character. Return total printed.
*/
int b_print(Buffer *const pBD)
{
	int printed = 0; /* tracks number of characters printed */
	char next; /* next char */
	if (!pBD) {
		return RT_FAIL_1;
	}
	if (!pBD->addc_offset) {
		return EMPTY;
	}
	while (1) {
		next = b_getc(pBD);
		if (b_eob(pBD)) {
			break;
		}
		printf("%c", next);
		printed++;
	}
	printf("\n");
	return printed;
}

/*
* Purpose: Shrinks, or expands, the buffer to a new capacity.
* Author: Brandon Gould
* Version: 0.1
* Called Functions: realloc()
* Parameters: Buffer * const pBD, char symbol.
* Return Value: NULL on errors, Buffer * const on success
* Algorithm: Check validity of Buffer * const. Generate new capacity. Allocate for new char buffer. 
*	Set R Flag for new address. Update capacity. Add symbol to buffer and inc addc_offset. Return buffer * const.
*/
Buffer * b_compact(Buffer * const pBD, char symbol)
{
	short newSpace; /* new capacity to error check before use */
	char* tempBuffer; /* temporary char pointer for chracter buffer */
	if (!pBD) {
		return NULL;
	}
	/* Generate new capacity */
	newSpace = pBD->addc_offset + 1;
	if (newSpace <= 0) {
		return NULL;
	}
	/* Allocate for new char buffer */
	tempBuffer = realloc(pBD->cb_head, newSpace);
	if (tempBuffer == NULL) {
		return NULL;
	}
	/* Set R flag for new address */
	if (tempBuffer != pBD->cb_head) {
		pBD->flags |= SET_R_FLAG;
		pBD->cb_head = tempBuffer;
	}
	/* set changed variables */
	pBD->capacity = newSpace;
	/* Add char, inc addc */
	pBD->cb_head[pBD->addc_offset++] = symbol;
	return pBD;
}

/*
* Purpose: Return the r_flag bit value.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: char value of r_flag, or RT error -1 (RT_FAIL_1).
* Algorithm: Check if Buffer is null. Check R flag, return as char.
*/
char b_rflag(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	return (char)(pBD->flags & CHECK_R_FLAG); /* Bitwise operation to get R Flag from Flags */
}

/*
* Purpose: Decrement getc_offset by 1.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD.
* Return Value: RT Error returns -1 (RT_FAIL_1). Success returns getc_offset (short).
* Algorithm: Check validity of Buffer*const. Deincrement getc_offset and return it.
*/
short b_retract(Buffer * const pBD) 
{
	if (!pBD || pBD->getc_offset == 0) {
		return RT_FAIL_1;
	}
	return --pBD->getc_offset;
}

/*
* Purpose: Set getc_offset to value of markc_offset.
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters:Buffer * const pBD
* Return Value: Short Buffer's new getc_offset, or RT_FAIL_1.
* Algorithm: Check validity of Buffer * const. Return getc_offset which is now set to markc_offset value.
*/
short b_reset(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	return pBD->getc_offset = pBD->markc_offset;
}

/*
* Purpose: Return getc_offset
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: Returns short (getc_offset). If RT error possibile (null Buffer* const) returns RT_FAIL_1(-1).
* Algorithm: Check Buffer * validitity. Return getc_offset.
*/
short b_getcoffset(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	return pBD->getc_offset;
}

/*
* Purpose: Sets the getc_offset and markc_offset to 0, so the buffer can be reread again. 
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer * const pBD
* Return Value: Int: error returns -1(RT_FAIL_1), else returns 0
* Algorithm: Check validity of Buffer Pointer. Set getc_offset and markc_offset to zero.
*/
int b_rewind(Buffer * const pBD)
{
	if (!pBD) {
		return RT_FAIL_1;
	}
	pBD->getc_offset = 0;
	pBD->markc_offset = 0;
	return SUCC;
}

/*
* Purpose: Return a pointer to location of the character buffer indicated by the current markc_offset
* Author: Brandon Gould
* Version: 0.1
* Called Functions:
* Parameters: Buffer* const pBD
* Return Value: Null on error, otherwise pointer to location of markc_offset
* Algorithm: Validity of Buffer Pointer. Return pointer to markc_offset location.
*/
char * b_location(Buffer * const pBD)
{
	if (!pBD) {
		return NULL;
	}
	//return &pBD->cb_head[pBD->markc_offset];
	return pBD->cb_head + pBD->markc_offset;
}