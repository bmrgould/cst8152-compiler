/*
 * File Name: scanner.c
 * Compiler: MS VS 2015
 * Authour: Brandon Gould 040 872 459
 * Assignment: 2
 * Date: 2019-03-18
 * Professor: Sv. Ranev
 * Purpose: SCANNER.C: Functions implementing a Lexical Analyzer (Scanner)
 *    as required for CST8152, Assignment #2
 *    scanner_init() must be called before using the scanner.
 * Function List: scanner_init, malar_next_token, char_class, aa_func02, aa_func03, aa_func05,
 * aa_func08, aa_func10, aa_func12, iskeyword
 */

/* The #define _CRT_SECURE_NO_WARNINGS should be used in MS Visual Studio projects
 * to suppress the warnings about using "unsafe" functions like fopen()
 * and standard sting library functions defined in string.h.
 * The define does not have any effect in Borland compiler projects.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>   /* standard input / output */
#include <ctype.h>   /* conversion functions */
#include <stdlib.h>  /* standard library functions and constants */
#include <string.h>  /* string functions */
#include <limits.h>  /* integer types constants */
#include <float.h>   /* floating-point types constants */

/*#define NDEBUG        to suppress assert() call */
#include <assert.h>  /* assert() prototype */

/* project header files */
#include "buffer.h"
#include "token.h"
#include "table.h"

#define DEBUG  /* for conditional processing */
#undef  DEBUG

/* Global objects - variables */
/* This buffer is used as a repository for string literals.
   It is defined in platy_st.c */
extern Buffer * str_LTBL; /*String literal table */
int line; /* current line number of the source code */
extern int scerrnum;     /* defined in platy_st.c - run-time error number */

/* Local(file) global objects - variables */
static Buffer *lex_buf;/*pointer to temporary lexeme buffer*/
static pBuffer sc_buf; /*pointer to input source buffer*/
/* No other global variable declarations/definitiond are allowed */

/* scanner.c static(local) function  prototypes */ 
static int char_class(char c); /* character class function */
static int get_next_state(int, char, int *); /* state machine function */
static int iskeyword(char * kw_lexeme); /*keywords lookup functuion */


/*Initializes scanner */
int scanner_init(Buffer * psc_buf) {
  	if(b_isempty(psc_buf)) return EXIT_FAILURE;/*1*/
	/* in case the buffer has been read previously  */
	b_rewind(psc_buf);
	b_clear(str_LTBL);
	line = 1;
	sc_buf = psc_buf;
	return EXIT_SUCCESS;/*0*/
/*   scerrnum = 0;  *//*no need - global ANSI C */
}

/*
* Purpose: Token recognition. 
* Author: Brandon Gould
* Version: 1.0
* Called Functions:
* Parameters: void
* Return Value: Token struc with it's code set, and attribute if appliciable.  
* Algorithm: While loop to go through all tokens/input.
*	1. Check Whitespace, New Lines, and End of File. 
*	2. Implementation of token driven scanner. Code for all special characters. 
*	3. Implementation of Transition Table scanner.
*	4. Handle Illegal Characters.
*/
Token malar_next_token (void) {
   Token t = {0}; /* token to return after pattern recognition. Set all structure members to 0 */
   unsigned char c; /* input symbol */
   int state = 0; /* initial state of the FSM */
   short lexstart;  /*start offset of a lexeme in the input char buffer (array) */
   short lexend;    /*end   offset of a lexeme in the input char buffer (array)*/
   int accept = NOAS; /* type of state - initially not accepting */                                        
        
   //DECLARE YOUR LOCAL VARIABLES HERE IF NEEDED   
   int value;
        while (1){ /* endless loop broken by token returns it will generate a warning */
                
        c = b_getc(sc_buf); /* GET THE NEXT SYMBOL FROM THE INPUT BUFFER */
		/* End of File? */
		if (c == EOL || c == SEOF) {
			t.code = SEOF_T;
			t.attribute.seof = SEOF_0; 
			return t;
		}

/* Part 1: Implementation of token driven scanner */              
/* every token is possessed by its own dedicated code*/

		if (c == ' ' || c == '\t' || c == '\v' || c == '\f') { /* White Space (Space, Tab, Vertical Tab, Feed) - based on isspace() characters  */
			continue;
		}
		else if (c == '\n' || c == '\r') { /* Line Terminators */
			if (c == '\r') {
				if (b_getc(sc_buf) == '\n') {
					line++;
					continue;
				}
				b_retract(sc_buf);
			}
			line++;
			continue;
		}  
		else if (c == '=') {
			if (b_getc(sc_buf) == '=') { /* Equal */
				t.code = REL_OP_T;
				t.attribute.rel_op = EQ;
				return t;
			}
			b_retract(sc_buf);
			t.code = ASS_OP_T; /* Assignment */
			return t;
		}
		else if (c == '(') { 
			t.code = LPR_T;
			return t;
		}
		else if (c == ')') {
			t.code = RPR_T;
			return t;
		}
		else if (c == '{') {
			t.code = LBR_T;
			return t;
		}
		else if (c == '}') {
			t.code = RBR_T;
			return t;
		}
		else if (c == '>') {
			t.code = REL_OP_T;
			t.attribute.rel_op = GT;
			return t;
		}
		else if (c == '<') {
			if (b_getc(sc_buf) == '>') { /* Not Equal */
				t.code = REL_OP_T;
				t.attribute.rel_op = NE;
				return t;
			}
			b_retract(sc_buf);
			if (b_getc(sc_buf) == '<') { /* concatentation or catenation */
				t.code = SCC_OP_T;
				return t;
			}
			b_retract(sc_buf);
			/* Less Than */
			t.code = REL_OP_T;
			t.attribute.rel_op = LT;
			return t;
		}
		else if (c == ';') {
			t.code = EOS_T;
			return t;
		}
		else if (c == ',') {
			t.code = COM_T;
			return t;
		}
		else if (c == '-') {
			t.code = ART_OP_T;
			t.attribute.arr_op = MINUS;
			return t;
		}
		else if (c == '+') {
			t.code = ART_OP_T;
			t.attribute.arr_op = PLUS;
			return t;
		}
		else if (c == '*') {
			t.code = ART_OP_T;
			t.attribute.arr_op = MULT;
			return t;
		}
		else if (c == '/') {
			t.code = ART_OP_T;
			t.attribute.arr_op = DIV;
			return t;
		}
		else if (c == '!') {
			value = 0;
			if (b_getc(sc_buf) != '!') { /* Not Comment */
				b_retract(sc_buf); /* Not a comment, error */
				t.code = ERR_T;
				t.attribute.err_lex[0] = c;
				t.attribute.err_lex[1] = b_getc(sc_buf);
				t.attribute.err_lex[2] = '\0';
				value = 1;
			}
			do {
				c = b_getc(sc_buf); /* Iterate through comment */
				if (c == EOL || c == SEOF) {
					t.code = SEOF_T;
					t.attribute.seof = SEOF_0;
					return t;
				}
			} while (c != '\n' && c != '\r'); /* Comment continues until end of line */
			if (c == '\r') {
				if (b_getc(sc_buf) == '\n') {
					
				} else {
					b_retract(sc_buf);
				}
			}
			line++;
			if (value == 1) {
				return t;
			}
			continue;
		}
		else if (c == '.') { /* .AND. .OR. */
			b_mark(sc_buf, b_getcoffset(sc_buf)); /* Set mark, for retract if error */
			/* AND */
			if (b_getc(sc_buf) == 'A') {
				if (b_getc(sc_buf) == 'N') {
					if (b_getc(sc_buf) == 'D') {
						if (b_getc(sc_buf) == '.') {
							/* AND */
							t.code = LOG_OP_T;
							t.attribute.log_op = AND;
							return t;
						}
						b_retract(sc_buf);
					}
					b_retract(sc_buf);
				}
				b_retract(sc_buf);
			}
			else { /* Else to make sure no weird case like .ANOR. passing */
				b_retract(sc_buf);
				if (b_getc(sc_buf) == 'O') {
					if (b_getc(sc_buf) == 'R') {
						if (b_getc(sc_buf) == '.') {
							/* OR */
							t.code = LOG_OP_T;
							t.attribute.log_op = OR;
							return t;
						}
						b_retract(sc_buf);
					}
					b_retract(sc_buf);
				}
			}
			b_retract(sc_buf);
			t.code = ERR_T;
			t.attribute.err_lex[0] = c; /* TODO Longer: count until fail -> for loop those chars? */
			t.attribute.err_lex[1] = '\0';
			return t;
		}      
/* Part 2: Implementation of Finite State Machine (DFA)
           or Transition Table driven Scanner 
           Note: Part 2 must follow Part 1 to catch the illegal symbols
*/  
		/* SET THE MARK AT THE BEGINING OF THE LEXEME AND SAVE IT IN lexstart */
		lexstart = b_mark(sc_buf, b_getcoffset(sc_buf)-1);
		state = 0;
		state = get_next_state(state, c, &accept);
		while (accept == NOAS) {
			c = b_getc(sc_buf);

			if (c == '\n' || c == '\r') { /* Line Terminators */
				if (c == '\r') {
					if (b_getc(sc_buf) == '\n') {
						line++;
					}
					else {
						b_retract(sc_buf);
					}
				}
				line++;
			}

			state = get_next_state(state, c, &accept);
		}
		/* Accepting, Token Found */
		if (accept == ASWR) {
			b_retract(sc_buf); /* Retract for retracting cases */
		}
		lexend = b_getcoffset(sc_buf);

		lex_buf = b_allocate((lexend-lexstart),0,'f');
		if (lex_buf == NULL) {
			scerrnum = 1;
			t.code = RTE_T;
			strcpy(t.attribute.err_lex, "RUN TIME ERROR: "); /* TODO: RTE messsage */
			return t;
		}
		b_reset(sc_buf); /* Retract getc_offset to mark */
		while (lexstart++ < lexend) {
			b_addc(lex_buf, b_getc(sc_buf)); /* Copy lexeme between start and end from input buffer to lex_buf */
		}
		lex_buf = b_compact(lex_buf,'\0'); 
		t = aa_table[state](b_location(lex_buf)); /* Call accepting function using the array aa_table */
		b_free(lex_buf);
		return t;
   }//end while(1)
}


/*
DO NOT MODIFY THE CODE OF THIS FUNCTION
YOU CAN REMOVE THE COMMENTS
*/
int get_next_state(int state, char c, int *accept)
{
	int col;
	int next;
	col = char_class(c);
	next = st_table[state][col];
#ifdef DEBUG
printf("Input symbol: %c Row: %d Column: %d Next: %d \n",c,state,col,next);
#endif
/*
The assert(int test) macro can be used to add run-time diagnostic to programs
and to "defend" from producing unexpected results.
assert() is a macro that expands to an if statement;
if test evaluates to false (zero) , assert aborts the program
(by calling abort()) and sends the following message on stderr:

Assertion failed: test, file filename, line linenum

The filename and linenum listed in the message are the source file name
and line number where the assert macro appears.
If you place the #define NDEBUG directive ("no debugging")
in the source code before the #include <assert.h> directive,
the effect is to comment out the assert statement.
*/
       assert(next != IS);

/*
The other way to include diagnostics in a program is to use
conditional preprocessing as shown bellow. It allows the programmer
to send more details describing the run-time problem. 
Once the program is tested thoroughly #define DEBUG is commented out
or #undef DEBUF is used - see the top of the file.
*/ 
#ifdef DEBUG
	if(next == IS){
	  printf("Scanner Error: Illegal state:\n");
	  printf("Input symbol: %c Row: %d Column: %d\n",c,state,col);
	  exit(1);
	}
#endif
	*accept = as_table[next];
	return next;
}

/*
* Purpose: Check given char to see which column of the transition table it belongs in.
* Author: Brandon Gould
* Version: 1.0
* Called Functions: isalpha()
* Parameters: char c - char to be checked.
* Return Value: Int from 0-6 representing the character's Transition Table column.
* Algorithm: Default on value 6 for 'Other'. If else If to check match for each column.
*/
int char_class (char c)
{
        int val = 6; /* Char's column number in transition table. Default 7 -> "Other" */
		/* 0: azAZ, 1:0, 2:1-9, 3:., 4:@, 5:", 6:Other */
		if (isalpha(c)) {
			val = 0; /* a-z A-Z*/
		}
		else if (c == '0') {
			val = 1; /* 0 */
		}
		else if (c > '0' && c <= '9') {
			val = 2; /* 1-9 */
		}
		else if (c == '.') {
			val = 3; /* . */
		}
		else if (c == '@') {
			val = 4; /* @ */
		} 
		else if (c == '"') {
			val = 5; /* " */
		}
		else if (c == EOL || c == SEOF) {
			val = 7; /* SEOF */
		}
		/* else it remains 6 for Other */
		return val;
}

/*
* Purpose: Accepting Function for Keywords.
* Author: Brandon Gould
* Version: 1.0
* Called Functions: iskeyword(), strlen(),
* Parameters: char lexeme[] - string accepted as keyword, to be handled and turned into a token.
* Return Value: Token
* Algorithm: Check if lexeme is a keyword through iskeyword(). Returns -1 if not a keyword, 0-9 if a keyword, referencing the index of the keyword in the table. 
If keyword, token code is KW_T. Token Attribute->kwt_idx is the index for the keyword. Return t (token).
If not keyword, token code is AVID_T for type AVID. vid_lex is limited by VID_LEN as max, with additional end of line '\0'.
Forloop for strlen of lexeme, and VID_LEN. Add each lexeme char at index to the token attribute vid_lex char at same index.
Add End of Line '\0', and return t token.
*/
Token aa_func02(char lexeme[]){
	Token t; /* New Token to be returned */
	unsigned int c; /* C ounter */
	int key; /* Keyword index */

	/* Keyword Check */
	key = iskeyword(lexeme);
	if (key >= 0) { /* Is checking if greater/equal to 0 better, or specifically checking for not -1? */
		t.code = KW_T; /* Token is type Keyword */
		t.attribute.kwt_idx = key;
		return t;
	}
	/* Not a keyword / AVID token */
	t.code = AVID_T; /* Token is type AVID */
	for (c = 0; c < strlen(lexeme) && c < VID_LEN; c++) {
		t.attribute.vid_lex[c] = lexeme[c]; 
	}
	t.attribute.vid_lex[c] = EOL;

  return t;
}


/*
* Purpose: Handle String Variable Identifiers (SVID). Set token code to SVID. 
* Author: Brandon Gould
* Version: 1.0
* Called Functions: strlen(),
* Parameters: char lexeme[] - string accepted as keyword, to be handled and turned into a token.
* Return Value: Token
* Algorithm: For length of lexeme, or until index reaches VID_LEN (max lenght of a vid), add lexeme char at index to token's attribute vid_lex at that index.
All SVID must end with '@', so if lexeme length is longer than VID_LEN the '@' will need to be replaced at SVID_LEN (VID_LEN -1).
Add EOL '\0' at end of vid_lex for c-type string. Using counter index, as it will be at end of vid_lex regardless.
Return token.
*/
Token aa_func03(char lexeme[]){
	Token t; /* new token */
	unsigned int c; /* C ounter */
	t.code = SVID_T;
	for (c = 0; c < strlen(lexeme) && c < VID_LEN; c++) {
		t.attribute.vid_lex[c] = lexeme[c];
	}
	if (strlen(lexeme) > VID_LEN) {
		t.attribute.vid_lex[SVID_LEN] = '@';
	}
	t.attribute.vid_lex[c] = '\0';
  return t;
}

/*
* Purpose: Accepting Function for Floating Point Literals.
* Author: Brandon Gould
* Version: 1.1
* Called Functions: atof(), strtof(), aa_func12()
* Parameters: char lexeme[] - string accepted as keyword, to be handled and turned into a token.
* Return Value: Token
* Algorithm: Use atof() to change string to double. Use double value to check if it fits inside a float.
* If it fits setup token as float, using strof() for float value from original lexeme.
* Errors are passed to aa_func12, and returned as the token.
*/
Token aa_func08(char lexeme[]){
	Token t; /* new token */
	double prefloat = atof(lexeme); /* lexeme converted to double, allowing checks before saving as float */
	/* Check if value is within float (4-bytes) value, and is shorter than 7 digits (4 bytes) */
	//if (  prefloat >= 0 && prefloat < FLT_MAX  || prefloat == 0.0 ) {
	if (prefloat >(-FLT_MAX) && prefloat > (-FLT_MIN) && prefloat < FLT_MAX && prefloat > FLT_MIN || prefloat == 0.0) {
		t.code = FPL_T;
		t.attribute.flt_value = strtof(lexeme, NULL);
		return t;
	}
	/* Otherwise error */
	return aa_table[ES](lexeme);
	//return aa_func12(lexeme);
}

/*
* Purpose: Accepting Function for integers.
* Author: Brandon Gould
* Version: 1.0
* Called Functions: strlen(), aa_func12()
* Parameters: char lexeme[] - string accepted as keyword, to be handled and turned into a token.
* Return Value: Token
* Algorithm: Convert lexeme into long, using it to check the value fits inside the range of int.
* If value does not fit it is sent to aa_func12 to make an error token, which is returned.
*/
Token aa_func05(char lexeme[]){
	Token t; /* new token */
	long preint;
	/* Check if number fits in int, but only necessary if no leading zero */
	/* Assuming 00000000000 is valid as 0: How is multiple zeros saved: just 0 */
	if ( lexeme[0] != '0' && strlen(lexeme) > INL_LEN) {
		//return aa_func12(lexeme); /* too long */
		return aa_table[ES](lexeme);
	}
	preint = atol(lexeme); /* lexeme into long for error checking */
	/* Error if not within range of int */
	if (preint < 0 || preint > SHRT_MAX) {
		//return aa_func12(lexeme); /* not within int range */
		return aa_table[ES](lexeme);
	}
	t.code = INL_T;
	t.attribute.int_value = (int)preint;
  return t;
}

/*
* Purpose: Accepting function for String Literals.
* Author: Brandon Gould
* Version: 1.0
* Called Functions: b_limit(), strlen(), b_addc(),
* Parameters: char lexeme[] - string accepted as keyword, to be handled and turned into a token.
* Return Value: Token
* Algorithm: Handle string literals. Ignore leading and closing quotes. Save the lexeme inside the String Literal Table.
* Use the token to track the start of the lexeme inside the table. 
*/
Token aa_func10(char lexeme[]){
	Token t;
	unsigned int c; /* c ounter */
	t.code = STR_T;
	t.attribute.str_offset = b_limit(str_LTBL); /* Start of where lexeme will be saved */
	for (c = 1; c < (strlen(lexeme)-1); c++) {
		b_addc(str_LTBL, lexeme[c]);
	}
	b_addc(str_LTBL, EOL);
  return t;
}

/*
* Purpose: Handle Error Tokens.
* Author: Brandon Gould
* Version: 1.0
* Called Functions: strlen(),
* Parameters: char lexeme[] - string accepted as keyword, to be handled and turned into a token.
* Return Value: Token
* Algorithm: Check length of lexeme to see if it fits within the error message. 
* If small enough, transer the whole lexeme. Otherwise tranfer what fits with additional ...
*/
Token aa_func12(char lexeme[]){
	Token t;
	unsigned int c; /* C ounter */
	int larger = 0; /* Flag for error logic. Default not larger. */
	unsigned int length;

	t.code = ERR_T;
	/*  Check if lexeme is larger than error length */
	if (strlen(lexeme) > ERR_LEN) {
		++larger;
	}
	/* Fill error message */
	length = strlen(lexeme);
	for (c = 0; c < length && c < ERR_LEN; c++) {
		if (larger && c > MAX_ERR_LEN) { /* If larger, need to make last 3 char ... */
			t.attribute.err_lex[c] = '.';
		}
		else {
			t.attribute.err_lex[c] = lexeme[c];
		}
	}
	t.attribute.vid_lex[c] = EOL;
  return t;
}

/*
* Purpose: Check if lexeme is a keyword.
* Author: Brandon Gould
* Version: 1.0
* Called Functions: strcmp(),
* Parameters: char lexeme[] - string accepted as keyword, to be handled and turned into a token.
* Return Value: int, negative meaning no matching keyword. Otherwise returns index of keyword.
* Algorithm: for loop keywords, compare strings.
*/
int iskeyword(char * kw_lexeme){
	int key;
	for (key = 0; key < KWT_SIZE; key++) {
		if (!strcmp(kw_table[key], kw_lexeme)) { /* strcmp returns 0 if equal */
			return key;
		}
	}
	return RT_FAIL_1;
}