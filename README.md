# CST8152-Compiler

Compiler from CST8152 course. Buffer, Scanner, Parser.

* Developed a buffer that can operate in three different modes; "fixed-size", "additive self-incrementing", and "multiplicative self-incrementing" buffers. 
* Developed a buffer implementation that is based on two associated data structures: a Buffer Descriptor (buffer control data structure) and an array of characters (the chracter buffer). Both structures are allocated dynamically.
* Created a transition table by converting the lexical grammar into regular expressions, then the regular expressions into a transition diagram, and then the transition diagram into the transition table. 
* Developed a scanner using a Deterministic Finite Automaton based on a Transition Table. 
* Developed a scanner that is a mixture between a transition-table driven and token driven scanner. 
* Transformed the provided LR grammar into LL grammar suitable for Recursive Descent Predictive Parsing. 
* Wrote the entire syntactic grammar and the corresponding FIRST sets.  
* Developed a Recursive Descent Predicitve Parser for the PLATYPUS lanaguage. 
* Intergrated the Parser with the exisiting lexical analyzer and symbol table to compelete the front-end of the PLATYPUS complier. 
