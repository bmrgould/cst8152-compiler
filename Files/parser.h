﻿/*
* File Name: parser.h
* Compiler: MS VS 2015
* Authour: Brandon Gould 040 872 459
* Assignment: 3
* Date: 2019-04-23
* Professor: Sv. Ranev
* Purpose: Header file for parser.c;
* Function List: parser(), match(), syn_eh(), syn_printe(), gen_incode(),program(void), opt_statements(void), statements(), statement_prime(), statement(), assignment_statement(), assignment_expression(), selection_statement(), iteration_statement(), pre_condition(), 
input_statement(), variable_list(), variable_list_prime(), variable_identifier(), output_statement(), output_list(), arithmetic_expression(), unary_arithmetic_expression(), additive_arithmetic_expression(), additive_arithmetic_expression_prime(), multiplicative_arithmetic_expression(), multiplicative_arithmetic_expression_prime(), primary_arithmetic_expression(), 
string_expression(), string_expression_prime(), primary_string_expression(), conditional_expression(), logical_or_expression(), logical_or_expression_prime(), logical_and_expression(), logical_and_expression_prime(), relational_expression(), comparison_operator(), primary_s_relational_expression(), primary_a_relational_expression();
*/

#ifndef PARSER_H_
#define PARSER_H_

#ifndef BUFFER_H_
#include "buffer.h"
#endif

#ifndef TOKEN_H_
#include "token.h"
#endif

/* Constants */
#define NO_ATTR -1

/* table index references */
#define ELSE 0
#define FALSE 1
#define IF 2
#define PLATYPUS 3
#define READ 4
#define REPEAT 5
#define THEN 6
#define TRUE 7
#define WHILE 8
#define WRITE 9

#define PLUS 0
#define MINUS 1
#define MULT 2
#define DIV 3

#define EQ 0
#define NE 1
#define GT 2
#define LT 3

#define AND 0
#define OR 1


/* Globals */
Token lookahead; 
int synerrno; 

extern Token malar_next_token(void);
extern int line;
extern Buffer * str_LTBL;
extern char * kw_table[];

/* Parser Function */
void parser(void);
void match(int pr_token_code, int pr_token_attribute);
void syn_eh(int sync_token_code);
void syn_printe();
void gen_incode(char* printthis);
/* Grammar Functions */
void program(void);
void opt_statements(void);
void statements();
void statement_prime();
void statement();
void assignment_statement();
void assignment_expression();
void selection_statement();
void iteration_statement();
void pre_condition();
void input_statement();
void variable_list();
void variable_list_prime();
void variable_identifier();
void output_statement();
void output_list();
void arithmetic_expression();
void unary_arithmetic_expression();
void additive_arithmetic_expression();
void additive_arithmetic_expression_prime();
void multiplicative_arithmetic_expression();
void multiplicative_arithmetic_expression_prime();
void primary_arithmetic_expression();
void string_expression();
void string_expression_prime();
void primary_string_expression();
void conditional_expression();
void logical_or_expression();
void logical_or_expression_prime();
void logical_and_expression();
void logical_and_expression_prime();
void relational_expression();
void comparison_operator();
void primary_s_relational_expression();
void primary_a_relational_expression();

#endif
