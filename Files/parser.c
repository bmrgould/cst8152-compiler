﻿/*
* File Name: parser.c
* Compiler: MS VS 2015
* Authour: Brandon Gould 040 872 459
* Assignment: 3
* Date: 2019-04-23
* Professor: Sv. Ranev
* Purpose: Parser utilitiy functions and parser functions
* Function List: parser(), match(), syn_eh(), syn_printe(), gen_incode(),program(void), opt_statements(void), statements(), statement_prime(), statement(), assignment_statement(), assignment_expression(), selection_statement(), iteration_statement(), pre_condition(), 
input_statement(), variable_list(), variable_list_prime(), variable_identifier(), output_statement(), output_list(), arithmetic_expression(), unary_arithmetic_expression(), additive_arithmetic_expression(), additive_arithmetic_expression_prime(), multiplicative_arithmetic_expression(), multiplicative_arithmetic_expression_prime(), primary_arithmetic_expression(), 
string_expression(), string_expression_prime(), primary_string_expression(), conditional_expression(), logical_or_expression(), logical_or_expression_prime(), logical_and_expression(), logical_and_expression_prime(), relational_expression(), comparison_operator(), primary_s_relational_expression(), primary_a_relational_expression();
*/

#include "parser.h"
#include <stdio.h>
#include <stdlib.h>

void parser(void) {
	lookahead = malar_next_token();
	program(); match(SEOF_T, NO_ATTR);
	gen_incode("PLATY: Source file parsed");
}

/*
* Purpose: Attempts to match two tokens (lookahead vs paramater token).
* Author: Brandon Gould
* Version: 1.0
* Called Functions: syn_eh(pr_token_code), malar_next_token(), syn_printe()
* Parameters: pr_token_code - int - represents Token Code to be matched
*				pr_token_attribute - int - represents Token Attribute of Token to be matched
* Return Value: void
* Algorithm: Check first match, comparing pr_token_code to lookahead.code, no match is error(syn_eh()). 
	Check SEOF for fast return.
	Switch of pr_token_code to check pr_token_attribute if needed. Failed match is error, syn_eh().
	Success gets next token, malar_next_token(), which is checked for error token (ERR_T) and handled if necessary.
*/
void match(int pr_token_code, int pr_token_attribute) {
	/* Check first match */
	if (pr_token_code != lookahead.code) {
		syn_eh(pr_token_code);
		return;
	}
	/* Matches SEOF */
	if (lookahead.code == SEOF_T) {
		return;
	}
	/* Check Second Match if needed */
	switch (pr_token_code) {
	case KW_T:
		if (pr_token_attribute != lookahead.attribute.kwt_idx) {
			syn_eh(pr_token_code);
			return;
		}
		break;
	case LOG_OP_T:
		if (pr_token_attribute != lookahead.attribute.log_op) {
			syn_eh(pr_token_code);
			return;
		}
		break;
	case ART_OP_T:
		if (pr_token_attribute != lookahead.attribute.arr_op) {
			syn_eh(pr_token_code);
			return;
		}
		break;
	case REL_OP_T:
		if (pr_token_attribute != lookahead.attribute.rel_op) {
			syn_eh(pr_token_code);
			return;
		}
		break;
	default: break;
	}
	/* Successful Matches - Continue */
	lookahead = malar_next_token();
	/* Check for Error Token */
	if (lookahead.code == ERR_T) {
		syn_printe();
		malar_next_token();
		++synerrno;
		return;
	}
}

/*
* Purpose: Error handling, simple panic mode error recovery.
* Author: Brandon Gould
* Version: 1.0
* Called Functions: syn_printe(), exit(), malar_next_token()
* Parameters: sync_token_code - int representing token to be matched
* Return Value: void
* Algorithm: syn_printe() and increment error counter.
*	While lookahead token does not match required token, check for end of file, increment token and check for match.
*	If end of file exit. If match, return if SEOF_T, otherwise incrment token then return.
*/
void syn_eh(int sync_token_code) {
	syn_printe();
	++synerrno;
	/* Advance until reaches correct token, or SEOF */
	while (sync_token_code != lookahead.code) {
		if (lookahead.code == SEOF_T) { 
			exit(synerrno);
			return;
		}
		lookahead = malar_next_token(); /* advance */
		if (sync_token_code == lookahead.code) { /* check for match */
			if (lookahead.code == SEOF_T) { /* match seof, dont advance */
				return;
			}
			lookahead = malar_next_token(); /* advance */
			return;
		}
	}
}

/* error printing function for Assignment 3 (Parser), W19 */
void syn_printe() {
	Token t = lookahead;

	printf("PLATY: Syntax error:  Line:%3d\n", line);
	printf("*****  Token code:%3d Attribute: ", t.code);
	switch (t.code) {
	case  ERR_T: /* ERR_T     0   Error token */
		printf("%s\n", t.attribute.err_lex);
		break;
	case  SEOF_T: /*SEOF_T    1   Source end-of-file token */
		printf("SEOF_T\t\t%d\t\n", t.attribute.seof);
		break;
	case  AVID_T: /* AVID_T    2   Arithmetic Variable identifier token */
	case  SVID_T:/* SVID_T    3  String Variable identifier token */
		printf("%s\n", t.attribute.vid_lex);
		break;
	case  FPL_T: /* FPL_T     4  Floating point literal token */
		printf("%5.1f\n", t.attribute.flt_value);
		break;
	case INL_T: /* INL_T      5   Integer literal token */
		printf("%d\n", t.attribute.get_int);
		break;
	case STR_T:/* STR_T     6   String literal token */
		b_mark(str_LTBL, t.attribute.str_offset);
		printf("%s\n", b_location(str_LTBL));
		break;

	case SCC_OP_T: /* 7   String concatenation operator token */
		printf("NA\n");
		break;

	case  ASS_OP_T:/* ASS_OP_T  8   Assignment operator token */
		printf("NA\n");
		break;
	case  ART_OP_T:/* ART_OP_T  9   Arithmetic operator token */
		printf("%d\n", t.attribute.get_int);
		break;
	case  REL_OP_T: /*REL_OP_T  10   Relational operator token */
		printf("%d\n", t.attribute.get_int);
		break;
	case  LOG_OP_T:/*LOG_OP_T 11  Logical operator token */
		printf("%d\n", t.attribute.get_int);
		break;

	case  LPR_T: /*LPR_T    12  Left parenthesis token */
		printf("NA\n");
		break;
	case  RPR_T: /*RPR_T    13  Right parenthesis token */
		printf("NA\n");
		break;
	case LBR_T: /*    14   Left brace token */
		printf("NA\n");
		break;
	case RBR_T: /*    15  Right brace token */
		printf("NA\n");
		break;

	case KW_T: /*     16   Keyword token */
		printf("%s\n", kw_table[t.attribute.get_int]);
		break;

	case COM_T: /* 17   Comma token */
		printf("NA\n");
		break;
	case EOS_T: /*    18  End of statement *(semi - colon) */
		printf("NA\n");
		break;
	default:
		printf("PLATY: Scanner error: invalid token code: %d\n", t.code);
	}/*end switch*/
}/* end syn_printe()*/

/* For Bonus if needed, for now just prints the string it is given */
void gen_incode(char* printthis) {
	printf("%s\n", printthis);
}

/* <program> -> PLATYPUS {<opt_statements>}
FIRST(<program>) = {KW_T(PLATYPUS)}
*/
void program(void) {
	match(KW_T, PLATYPUS); match(LBR_T, NO_ATTR); opt_statements();
	match(RBR_T, NO_ATTR);
	gen_incode("PLATY: Program parsed");
}

/* <opt_statements> -> <statements> | ε
FIRST(<opt_statements>) = { ε, AVID_T, SVID_T, KW_T(IF), KW_T(WHILE), KW_T(READ), KW_T(WRITE) }
*/
void opt_statements() {
	switch (lookahead.code) {
	case AVID_T:
	case SVID_T: statements(); break;
	case KW_T:
		if (lookahead.attribute.get_int == IF || lookahead.attribute.get_int == WHILE || lookahead.attribute.get_int == READ || lookahead.attribute.get_int == WRITE) {
			statements();
		}
		break;
	default: /*Empty string, optional statements */;
		gen_incode("PLATY: Opt_statements parsed");
	}
	
}

/* <statements> -> <statement><statements’>
FIRST(<statements>) = { AVID_T, SVID_T, KW_T(IF), KW_T(WHILE), KW_T(READ), KW_T(WRITE) }
*/
void statements() {
	statement();
	statement_prime();
}

/* <statements’> -> <statement><statements’> | ε
FIRST(<statements'>) = { ε, AVID_T, SVID_T, KW_T(IF), KW_T(WHILE), KW_T(READ), KW_T(WRITE) }
*/
void statement_prime() {
	switch (lookahead.code) {
	case AVID_T:
	case SVID_T:
		statement();
		statement_prime();
	case KW_T:
		switch (lookahead.attribute.kwt_idx) {
		case IF: case WHILE: case READ: case WRITE:
			statement();
			statement_prime();
		}
		break;
	default: /* empty */
		break;
	}
}

/*
<statement> ->
<assignment statement> | <selection statement> | <iteration statement> |
<input statement> | <output statement>

FIRST(<statement>) = {FIRST(<assignment statement>), FIRST(<selection statement>), FIRST(<iteration statement>), FIRST(<input statement>), FIRST(<output statement>)}
= { AVID_T, SVID_T, KW_T(IF), KW_T(WHILE), KW_T(READ), KW_T(WRITE) }

*/
void statement() {
	switch (lookahead.code) {
	case AVID_T:
	case SVID_T:
		assignment_statement();
		break;
	case KW_T:
		switch (lookahead.attribute.get_int) {
		case IF:
			selection_statement(); break;
		case WHILE:
			iteration_statement(); break;
		case READ:
			input_statement(); break;
		case WRITE:
			output_statement(); break;
		default: 
			syn_printe();
		}
		break;
	default:
		syn_printe();
	}
}

/* <assignment statement> -> <assignment expression>; 
FIRST(<assignment statement>) = { FIRST(<assignment expression>)} = {AVID_T , SVID_T}
*/
void assignment_statement() {
	assignment_expression();
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: Assignment statement parsed");
}

/* <assignment expression> -> AVID = <arithmetic expression> | SVID = <string expression>
FIRST(<assignment expression>) = {AVID_T , SVID_T}
*/
void assignment_expression() {
	switch (lookahead.code) {
	case AVID_T:
		match(AVID_T, NO_ATTR);
		match(ASS_OP_T, EQ);
		arithmetic_expression();
		gen_incode("PLATY: Assignment expression (arithmetic) parsed");
		break;
	case SVID_T:
		match(SVID_T, NO_ATTR);
		match(ASS_OP_T, EQ);
		string_expression();
		gen_incode("PLATY: Assignment expression (string) parsed");
		break;
	default:
		syn_printe();
	}
}

/* <selection statement> ->
	IF <pre-condition> (<conditional expression>) THEN { <opt_statements> }
	ELSE { <opt_statements> };
FIRST(<selection statement>) = {KW_T(IF)}
*/
void selection_statement() {
	match(KW_T, IF);
	pre_condition();
	match(LPR_T, NO_ATTR);
	conditional_expression();
	match(RPR_T, NO_ATTR);
	match(KW_T, THEN);
	match(LBR_T, NO_ATTR);
	opt_statements();
	match(RBR_T, NO_ATTR);
	match(KW_T, ELSE);
	match(LBR_T, NO_ATTR);
	opt_statements();
	match(RBR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: Selection statement parsed");
}

/* <iteration statement> -> 
	WHILE <pre-condition> (<conditional expression>)
	REPEAT {<statements>};
	FIRST(<iteration statement>) = {KW_T(WHILE)}
*/
void iteration_statement() {
	match(KW_T, WHILE);
	pre_condition();
	match(LPR_T, NO_ATTR);
	conditional_expression();
	match(RPR_T, NO_ATTR);
	match(KW_T, REPEAT);
	match(LBR_T, NO_ATTR);
	statements();
	match(RBR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: Iteration statement parsed");
}

/* <pre-condition> -> TRUE | FALSE
FIRST(<pre-condition>) = {KW_T(TRUE), KW_T(FALSE)}
*/
void pre_condition() {
	switch (lookahead.code) {
	case KW_T:
		switch (lookahead.attribute.get_int) { // TODO: TRUE FALSE what attribute?
		case TRUE:
			match(KW_T, TRUE);
			break;
		case FALSE:
			match(KW_T, FALSE);
			break;
		default:
			syn_printe();
			break;
		}
		break;
	default:
		syn_printe();
	}
}

/* <input statement> -> READ (<variable list>);
FIRST(<input statement>) = {KW_T(READ)}
*/
void input_statement() {
	match(KW_T, READ);
	match(LPR_T, NO_ATTR);
	variable_list();
	match(RPR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: Input statement parsed");
}

/* <variable list> -> <variable identifier><variable list’>
FIRST(<variable list>) = {FIRST(<variable identifier>)} = {AVID_T, SVID_T}
*/
void variable_list() {
	variable_identifier();
	variable_list_prime();
	gen_incode("PLATY: Variable list parsed");
}

/* <variable list’> -> ,<variable identifier><variable list’> | ε
FIRST(<variable list’>) = { ε, COM_T}
*/
void variable_list_prime() {
	switch (lookahead.code) {
	case COM_T:
		match(COM_T, NO_ATTR);
		variable_identifier();
		variable_list_prime();
		break;
	default: /* empty? */
		break;
	}
}

/* <variable identifier> -> AVID_T | SVID_T
FIRST(<variable identifier>) = {AVID_T, SVID_T}
*/
void variable_identifier() {
	switch (lookahead.code) {
	case AVID_T:
		match(AVID_T, NO_ATTR);
		break;
	case SVID_T:
		match(SVID_T, NO_ATTR);
		break;
	default:
		syn_printe();
	}
}

/* <output statement> -> WRITE (<output list>);
FIRST(<output statement>) = {KW_T(WRITE)}
*/
void output_statement() {
	match(KW_T, WRITE);
	match(LPR_T, NO_ATTR);
	output_list();
	match(RPR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: Output statement parsed");
}

/* <output list> -> <opt_variable list> | STR_T
FIRST(<output list>) = {STR_T, FIRST(<opt_variable list>)} = { STR_T,  ε, AVID_T, SVID_T}
*/
void output_list() {
	switch (lookahead.code) {
	case STR_T:
		match(STR_T, NO_ATTR);
		gen_incode("PLATY: Output list (string literal) parsed");
		break;
	case AVID_T:
	case SVID_T:
		variable_list();
		break;
	default:
		gen_incode("PLATY: Output list (empty) parsed");
		break;
	}
}

/*<arithmetic expression> -> 
<unary arithmetic expression> | <additive arithmetic expression>

FIRST(<arithmetic expression>) 
= {FIRST(unary arithmetic expression), FIRST(additive arithmetic expression)} 
= {AVID_T, FPL_T, INL_T, LPR_T, ART_OP_T(PLUS), ART_OP_T(MINUS)}
*/
void arithmetic_expression() {
	switch (lookahead.code) {
	case AVID_T:
	case FPL_T:
	case INL_T:
	case LPR_T:
		additive_arithmetic_expression();
		break;
	case ART_OP_T:
		switch (lookahead.attribute.arr_op) {
		case PLUS:
		case MINUS:
			unary_arithmetic_expression();
			break;
		default:
			syn_printe();
		}
		break;
	default:
		syn_printe();
		return;
	}
	gen_incode("PLATY: Arithmetic expression parsed");
}

/* <unary arithmetic expression> ->
	- <primary arithmetic expression> | + <primary arithmetic expression>
	FIRST(<unary arithmetic expression>) = {ART_OP_T(PLUS), ART_OP_T(MINUS)}
*/
void unary_arithmetic_expression() {
	switch (lookahead.code) {
	case ART_OP_T:
		switch (lookahead.attribute.arr_op) {
		case PLUS:
			match(ART_OP_T, PLUS);
			primary_arithmetic_expression();
			break;
		case MINUS:
			match(ART_OP_T, MINUS);
			primary_arithmetic_expression();
			break;
		default:
			syn_printe();
			break;
		}
		break;
	default:
		syn_printe();
		break;
	}
	gen_incode("PLATY: Unary arithmetic expression parsed");
}

/* <add. A. E.> -> <multi. A.E.> <add. A. E.’>
FIRST(<additive arithmetic expression>) = {FIRST(multiplicative arithmetic expression)} = {AVID_T, FPL_T, INL_T, LBR_T}
*/
void additive_arithmetic_expression() {
	multiplicative_arithmetic_expression();
	additive_arithmetic_expression_prime();
}

/* <add. A. E.’> -> + <multi. A. E.> <add. A. E.’> | - <multi. A. E.> <add. A. E.’> | ε
FIRST(<additive arithmetic expression’>) = { ε, ART_OP_T(PLUS), ART_OP_T(MINUS)}
*/
void additive_arithmetic_expression_prime() {
	switch (lookahead.code) {
	case ART_OP_T:
		switch (lookahead.attribute.arr_op) {
		case PLUS:
			match(ART_OP_T, PLUS);
			multiplicative_arithmetic_expression();
			additive_arithmetic_expression_prime();
			break;
		case MINUS:
			match(ART_OP_T, MINUS);
			multiplicative_arithmetic_expression();
			additive_arithmetic_expression_prime();
			break;
		default:
			syn_printe();
			return;
		}
	default: /* empty */
		break;
	}
	gen_incode("PLATY: Additive arithmetic expression parsed");
}

/* <multi. A. E.> -> <pri A. E.> <multi. A. E.’>
FIRST(<multiplicative arithmetic expression>) = {FIRST(<primary arithmetic expression>)} 
= {AVID_T, FPL_T, INL_T, LBR_T}
*/
void multiplicative_arithmetic_expression() {
	primary_arithmetic_expression();
	multiplicative_arithmetic_expression_prime();
}

/*<multi. A. E.’> -> * <pri A. E.> <multi. A. E.’> | / <pri A. E.> <multi. A. E.’> | ε
FIRST(<multiplicative arithmetic expression’>) = {  ε, ART_OP_T(MULT), ART_OP_T(DIV) }
*/
void multiplicative_arithmetic_expression_prime() {
	switch (lookahead.code) {
	case ART_OP_T:
		switch (lookahead.attribute.arr_op) {
		case MULT:
			match(ART_OP_T, MULT);
			primary_arithmetic_expression();
			multiplicative_arithmetic_expression_prime();
			break;
		case DIV:
			match(ART_OP_T, DIV);
			primary_arithmetic_expression();
			multiplicative_arithmetic_expression_prime();
			break;
		default: 
			return;
		}
		break;
	default: /* empty */
		return;
	}
	gen_incode("PLATY: Multiplicative arithmetic expression parsed");
}

/* <primary arithmetic expression> -> AVID_T | FPL_T | INL_T | (<arithmetic expression>)
FIRST(<primary arithmetic expression> ) = { AVID_T, FPL_T, INL_T, LBR_T }
*/
void primary_arithmetic_expression() {
	switch (lookahead.code) {
	case AVID_T:
		match(AVID_T, NO_ATTR);
		break;
	case FPL_T:
		match(FPL_T, NO_ATTR);
		break;
	case INL_T:
		match(INL_T, NO_ATTR);
		break;
	case LPR_T:
		match(LPR_T, NO_ATTR);
		arithmetic_expression();
		match(RPR_T, NO_ATTR);
		break;
	default: 
		syn_printe();
		return;
	}
	gen_incode("PLATY: Primary arithmetic expression parsed");
}

/* <string expression> -> <primary string expression> <string expression’>
FIRST(<string expression>) = { FIRST(primary string expression) } = { SVID_T, STR_T }
*/
void string_expression() {
	primary_string_expression();
	string_expression_prime();
}

/* <string expression’> -> << <primary string expression> <string expression’> | ε
FIRST(<string expression’>) = { REL_OP_T(LT), ε  }
*/
void string_expression_prime() {
	switch (lookahead.code) {
	case REL_OP_T:
		if (lookahead.attribute.rel_op == LT){
			match(REL_OP_T, LT);
			primary_string_expression();
			string_expression_prime();
		}
		break;
	default: /* empty */
		break;
	}
}

/* <primary string expression> -> SVID_T | STR_T
FIRST(<primary string expression>) = { SVID_T, STR_T }
*/
void primary_string_expression() {
	switch (lookahead.code) {
	case SVID_T:
		match(SVID_T, NO_ATTR);
		break;
	case STR_T:
		match(STR_T, NO_ATTR);
		break;
	default:
		syn_printe();
		return;
	}
	gen_incode("PLATY: Primary string expression parsed");
}

/* <conditional expression> -> <logical OR expression>

FIRST(<conditional expression>) = { FIRST(<Logical OR expression>) }
= { AVID_T, FPL_T, INL_T, SVID_T, STR_T }

*/
void conditional_expression() {
	logical_or_expression();
	gen_incode("PLATY: Conditional expression parsed");
}

/* <logical OR exp.> -> <logical AND exp.> <logical OR exp.’>
FIRST(<logical OR expression>) = { FIRST(Logical AND expression) }
= { AVID_T, FPL_T, INL_T, SVID_T, STR_T }

*/
void logical_or_expression() {
	logical_and_expression();
	logical_or_expression_prime();
}

/* <logical OR exp.’> -> .OR. <logical AND exp.>  <logical OR exp.’> | ε
FIRST(<logical OR expression’>) = {  ε, LOG_OP_T(OR)  }
*/
void logical_or_expression_prime() {
	switch (lookahead.code) {
	case LOG_OP_T:
		if (lookahead.attribute.log_op == OR) {
			match(LOG_OP_T, OR);
			logical_and_expression();
			logical_or_expression_prime();
			gen_incode("PLATY: Logical OR expression parsed");
			break;
		}
	default: /* empty */
		break;
	}
}

/*  <logical AND exp.> -> <relational exp.> <logical AND exp.’>
FIRST(<logical AND expression>) = { FIRST(relational expression) }
= { AVID_T, FPL_T, INL_T, SVID_T, STR_T }

*/
void logical_and_expression() {
	relational_expression();
	logical_and_expression_prime();
}

/* <logical AND exp’> -> .AND. <relational exp.> <logical AND exp.’> | ε
FIRST(<logical AND expression’>) = { ε, LOG_OP_T(AND) }
*/
void logical_and_expression_prime() {
	switch (lookahead.code) {
	case LOG_OP_T:
		if (lookahead.attribute.log_op == AND) {
			match(LOG_OP_T, AND);
			relational_expression();
			logical_and_expression_prime();
			gen_incode("PLATY: Logical AND expression parsed");
		}
		break;
	default: /* empty */
		break;
	}
}

/* <relational expression> ->
		<primary a_relational exp.> <comparison operator> <primary a_relational exp.>
		| <primary s_relational exp.> <comparison operator> <primary s_relational exp.>
FIRST(<relational expression>) = { FIRST(<primary a_relational expression>), FIRST(<primary s_relational expression>) } 
= { AVID_T, FPL_T, INL_T, SVID_T, STR_T }
*/
void relational_expression() {
	switch (lookahead.code) {
	case AVID_T:
	case FPL_T:
	case INL_T:
		primary_a_relational_expression();
		comparison_operator();
		primary_a_relational_expression();
		break;
	case SVID_T:
	case STR_T:
		primary_s_relational_expression();
		comparison_operator();
		primary_s_relational_expression();
		break;
	default: 
		syn_printe();
		return;
	}
	gen_incode("PLATY: Relational expression parsed");
}

/* <comparison operator> -> == | <> | > | <
FIRST(<comparison operator>) = { REL_OP_T(EQ), REL_OP_T(NE), REL_OP_T(GT), REL_OP_T(LT) }
*/
void comparison_operator() {
	switch (lookahead.code) {
	case REL_OP_T:
		switch (lookahead.attribute.rel_op) {
		case EQ:
			match(REL_OP_T, EQ);
			break;
		case NE:
			match(REL_OP_T, NE);
			break;
		case GT:
			match(REL_OP_T, GT);
			break;
		case LT:
			match(REL_OP_T, LT);
			break;
		default: /* bad? */
			syn_printe();
			return;
		}
		break;
	default: 
		break;
	}
}

/* <primary s_relational expression> -> <primary string expression>
FIRST(<primary s_relational expression>) = { FIRST(<primary string expression>) } = { SVID_T, STR_T }
*/
void primary_s_relational_expression() {
	primary_string_expression();
	gen_incode("PLATY: Primary s_relational expression parsed");
}

/* <primary a_relational expression> -> AVID_T | FPL_T | INL_T
FIRST(<primary a_relational expression>) = { AVID_T, FPL_T, INL_T }
*/
void primary_a_relational_expression() {
	switch (lookahead.code) {
	case AVID_T:
		match(AVID_T, NO_ATTR);
		break;
	case FPL_T:
		match(FPL_T, NO_ATTR);
		break;
	case INL_T:
		match(INL_T, NO_ATTR);
		break;
	default: 
		syn_printe();
		return;
	}
	gen_incode("PLATY: Primary a_relational expression parsed");
}